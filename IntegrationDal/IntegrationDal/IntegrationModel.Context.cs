﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegrationDal
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Entities : DbContext
    {
        public Entities()
            : base("name=Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Ontraport_Contact_Updates> Ontraport_Contact_Updates { get; set; }
        public virtual DbSet<Zendesk_Kyc_Updates> Zendesk_Kyc_Updates { get; set; }
        public virtual DbSet<Zendesk_Contact_Updates> Zendesk_Contact_Updates { get; set; }
    }
}
