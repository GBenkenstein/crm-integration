﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegrationDal
{
    public partial class Entities
    {
        private Entities(EntityConnection connection)
            : base(connection, true)
        {

        }

        public static Entities CreateFromConnectionString(string connectionString)
        {
            //SqlConnectionStringBuilder stringbuilder = new SqlConnectionStringBuilder(connectionString);
            //EntityConnectionStringBuilder efConnectionStringBuilder = new EntityConnectionStringBuilder();
            //efConnectionStringBuilder.Provider = "System.Data.SqlClient";
            //efConnectionStringBuilder.ProviderConnectionString = stringbuilder.ToString();
            //efConnectionStringBuilder.Metadata = "res://*/Dal.WealthMigrateIntegration.csdl|res://*/Dal.WealthMigrateIntegration.ssdl|res://*/Dal.WealthMigrateIntegration.msl";
            EntityConnection connection = new EntityConnection(connectionString);
            //DbConfiguration.SetConfiguration(new DalConfiguration());
            return new Entities(connection);
        }
    }
}
