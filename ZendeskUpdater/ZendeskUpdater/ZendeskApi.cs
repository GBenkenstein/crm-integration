﻿using IntegrationDal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZendeskUpdater
{
    public class ZendeskApi:MessageObject
    {
        protected static HttpClient client = new HttpClient();
        

        public ZendeskApi(string username, string zendeskToken)
        {
            ServicePointManager.DefaultConnectionLimit = 500;
            var byteArray = Encoding.ASCII.GetBytes($@"{username}/token:{zendeskToken}");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("basic", Convert.ToBase64String(byteArray));
            //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
        }

        public ZendeskApi(string bearerToken)
        {
            ServicePointManager.DefaultConnectionLimit = 500;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", bearerToken);
            client.DefaultRequestHeaders.Add("User-Agent", @"ZendeskIntegration");
            client.DefaultRequestHeaders.Add("Accept", @"*/*");
        }

        public async Task<HttpResponseMessage> GetAsync(string requestUri)
        {
            var response = await client.GetAsync(requestUri);
            if ((int)response.StatusCode == 429)
            {
                Wait(response);
                return await GetAsync(requestUri);
            }
            return response;
        }

        private void Wait(HttpResponseMessage response)
        {
            int waitInterval;
            if (response.Headers.Contains("Retry-After"))
                waitInterval = int.Parse(response.Headers.GetValues("Retry-After").First());
            else
                waitInterval = 1;
            OnMessageRaised(this, $@"Zendesk rate limit hit. Sleeping for {waitInterval} seconds.");
            Thread.Sleep(waitInterval * 1000);
        }

        public async Task<HttpResponseMessage> PostAsync(string requestUri, string content)
        {
            var response = await client.PostAsync(requestUri, new StringContent(content, Encoding.UTF8, @"application/json"));
            if ((int)response.StatusCode == 429)
            {
                Wait(response);
                return await PostAsync(requestUri, content);
            }
            return response;
        }

        public async Task<HttpResponseMessage> PutAsync(string requestUri, string content)
        {
            var response = await client.PutAsync(requestUri, new StringContent(content, Encoding.UTF8, @"application/json"));
            if ((int)response.StatusCode == 429)
            {
                Wait(response);
                return await PostAsync(requestUri, content);
            }
            return response;
        }
    }
}
