﻿using IntegrationDal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ZendeskUpdater
{
    public class ZendeskSellUpdater:MessageObject
    {
        protected Entities Context;
        private int batchsize;
        private int leadRouterOwnerId = 0;
        private int contactRouterOwnerId = 0;
        private ZendeskApi api;

        public ZendeskSellUpdater(string zendeskBearerToken, string connectionString, int batchsize, string leadRouterName = null, string contactRouterName = null)
        {
            this.batchsize = batchsize;
            
            DbConfiguration.SetConfiguration(new DalConfiguration());
            Context = Entities.CreateFromConnectionString(connectionString);

            api = new ZendeskApi(zendeskBearerToken);
            api.MessageRaised += Api_MessageRaised;

            //lookup lead and contact router ids
            var response = api.GetAsync($@"https://api.getbase.com/v2/users?system_tags=lead_distribution");
            if (response.Result.IsSuccessStatusCode)
            {
                var data = JsonConvert.DeserializeObject<JObject>(response.Result.Content.ReadAsStringAsync().Result);
                leadRouterOwnerId = (int)data["items"][0]["data"]["id"];
            }
            response = api.GetAsync($@"https://api.getbase.com/v2/users?system_tags=contact_distribution");
            if (response.Result.IsSuccessStatusCode)
            {
                var data = JsonConvert.DeserializeObject<JObject>(response.Result.Content.ReadAsStringAsync().Result);
                contactRouterOwnerId = (int)data["items"][0]["data"]["id"];
            }
        }

        private void Api_MessageRaised(object sender, string e)
        {
            OnMessageRaised(sender, e);
        }

        public void ProcessUpdates()
        {
            var auditKeyQuery = from item in Context.Zendesk_Contact_Updates
                                where item.SentToZendeskIndicator == false
                                group item by item.AuditKey into auditkeys
                                select auditkeys.Key;
            var auditKeys = auditKeyQuery.ToArray();
            foreach (int auditkey in auditKeys)
            {
                //First do leads,
                var query = from item in Context.Zendesk_Contact_Updates
                            where item.AuditKey == auditkey
                                && item.SentToZendeskIndicator == false
                                && item.ContactType == "lead"
                            orderby item.Id
                            select item;

                BatchProcessor<Zendesk_Contact_Updates> processor = new BatchProcessor<Zendesk_Contact_Updates>(batchsize);
                processor.ProcessBatches(Context, query, SendLeadUpdate);

                Thread.Sleep(1000); //Need to give Zendesk API some time to finish processing 

                //Then do trusts and companies
                query = from item in Context.Zendesk_Contact_Updates
                        where item.AuditKey == auditkey
                            && item.SentToZendeskIndicator == false
                            && (item.ContactType == "trust" || item.ContactType == "company")
                        orderby item.Id
                        select item;

                processor = new BatchProcessor<Zendesk_Contact_Updates>(batchsize);
                processor.ProcessBatches(Context, query, SendContactUpdateAsync);

                Thread.Sleep(1000); //Need to give Zendesk API some time to finish processing
                                    //Then do personal
                query = from item in Context.Zendesk_Contact_Updates
                        where item.AuditKey == auditkey
                            && item.SentToZendeskIndicator == false
                            && item.ContactType == "personal"
                        orderby item.Id
                        select item;

                processor.ProcessBatches(Context, query, SendContactUpdateAsync);
            }


        }

        private async Task SendContactUpdateAsync(Zendesk_Contact_Updates item)
        {
            try
            {
                //Check to see if there is a company with the same login email
                int companyContactId = 0;
                var response = await api.GetAsync($@"https://api.getbase.com/v2/contacts?is_organization=true&custom_fields[Platform Login Email]={item.PlatformLoginEmail}");


                if (response.IsSuccessStatusCode)
                {
                    var companydata = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                    if (companydata["items"].Count() > 0)
                    {
                        companyContactId = (int)companydata["items"][0]["data"]["id"];
                    }
                }

                var jsonObject = new
                {
                    data = new
                    {
                        first_name = item.FirstName,
                        last_name = item.LastName == string.Empty ? "Unknown" : item.LastName,
                        name =  item.LastName == string.Empty ? "Unknown" : item.LastName,
                        email = item.ContactEmail,
                        phone = item.ContactNumber,
                        mobile = item.MobileNumber,
                        is_organization = string.Equals(item.ContactType, "company", StringComparison.OrdinalIgnoreCase) || string.Equals(item.ContactType, "trust", StringComparison.OrdinalIgnoreCase) ? true : false,
                        contact_id = companyContactId == 0 ? (int?)null : companyContactId,
                        customer_status = item.PlatformInvestorFlag.Value ? "current" : null,
                        owner_id = contactRouterOwnerId != 0 ? contactRouterOwnerId : (int?)null,
                        custom_fields = new Dictionary<string, object>()
                        {
                            {"Subscribe to investment opportunities", item.SubscribeToInvestmentOpportunities.Value },
                            {"Subscribe to marketing messages", (bool)item.SubscribeToMarketingMessages.Value },
                            {"Platform Entity Country", item.Country },
                            {"Platform KYC Status", item.KycStatus },
                            {"Platform Member", item.PlatformMemberFlag.Value},
                            {"Platform Investor", item.PlatformInvestorFlag.Value},
                            {"Platform Repeat Investor", item.PlatformRepeatInvestorFlag.Value},
                            {"Platform Wallet Balance", item.WalletBalance.Value },
                            {"Platform Login Email", item.PlatformLoginEmail }
                        }
                    },

                };
                string jsonString = JsonConvert.SerializeObject(jsonObject, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                //Check to see if the contact already exists
                response = await api.GetAsync($@"https://api.getbase.com/v2/contacts?email={item.ContactEmail}");
                var data = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                if (data["items"].Count() == 0)
                {
                    //Check to see if a lead needs to be converted
                    response = await api.GetAsync($@"https://api.getbase.com/v2/leads?email={item.PlatformLoginEmail}");
                    data = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                    if (data["items"].Count() == 0)
                    {
                        //Create new contact
                        await CreateContactAsync(item, jsonString);
                    }
                    else
                    {
                        if (string.Equals(item.ContactType, "company", StringComparison.OrdinalIgnoreCase) || string.Equals(item.ContactType, "trust"))
                            await ConvertLeadToCompanyAsync(item, jsonString, response, data);
                        else
                            await ConvertLeadToPersonContactAsync(item, jsonString, response, data);
                    }
                }
                else
                {
                    //Update the contact data
                    int contactId = (int)data["items"][0]["data"]["id"];
                    await UpdateContactAsync(item, jsonString, contactId);
                }
            }
            catch (Exception e)
            {
                OnMessageRaised(this, e.Message);
                throw e;
            }
        }

        private async Task ConvertLeadToPersonContactAsync(Zendesk_Contact_Updates item, string jsonString, HttpResponseMessage response, JObject data)
        {
            //Update the lead
            int leadid = (int)data["items"][0]["data"]["id"];
            var leadObject = new
            {
                data = new
                {
                    first_name = item.FirstName,
                    last_name = item.LastName == string.Empty ? "Unknown" : item.LastName,
                    organization_name = "",
                },
            };
            string leadJsonString = JsonConvert.SerializeObject(leadObject, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            response = await api.PutAsync($@"https://api.getbase.com/v2/leads/{leadid}", leadJsonString);
            if (response.IsSuccessStatusCode)
            {
                //Convert the lead
                var leadConversionData = new
                {
                    data = new
                    {
                        lead_id = leadid,
                        create_deal = false
                    }
                };
                string leadConvertDataString = JsonConvert.SerializeObject(leadConversionData, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                response = await api.PostAsync($@"https://api.getbase.com/v2/lead_conversions", leadConvertDataString);
                if (response.IsSuccessStatusCode)
                {
                    OnMessageRaised(this, $@"Converted lead for {item.ContactEmail}");
                    //Get the newly created Contact's Id and update with the latest data
                    var conversiondata = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                    int contactId = (int)conversiondata["data"]["individual_id"];
                    await UpdateContactAsync(item, jsonString, contactId);
                }
            }
            else
            {
                OnErrorMessageRaised(response);
            }
        }

        private async Task ConvertLeadToCompanyAsync(Zendesk_Contact_Updates item, string jsonString, HttpResponseMessage response, JObject data)
        {
            //Update the lead
            int leadid = (int)data["items"][0]["data"]["id"];
            var leadObject = new
            {
                data = new
                {
                    first_name = "",
                    last_name = "",
                    organization_name = item.LastName == string.Empty ? "Unknown" : item.LastName,
                },
            };
            string leadJsonString = JsonConvert.SerializeObject(leadObject, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            response = await api.PutAsync($@"https://api.getbase.com/v2/leads/{leadid}", leadJsonString);
            if (response.IsSuccessStatusCode)
            {
                //Convert the lead
                var leadConversionData = new
                {
                    data = new
                    {
                        lead_id = leadid,
                        create_deal = false
                    }
                };
                string leadConvertDataString = JsonConvert.SerializeObject(leadConversionData, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                response = await api.PostAsync($@"https://api.getbase.com/v2/lead_conversions", leadConvertDataString);
                if (response.IsSuccessStatusCode)
                {
                    //Get the newly created Contact's Id and update with the latest data
                    var conversiondata = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                    int contactId = (int)conversiondata["data"]["organization_id"];
                    await UpdateContactAsync(item, jsonString, contactId);
                }
            }
            else
            {
                OnErrorMessageRaised(response);
            }

        }


        private async Task CreateContactAsync(Zendesk_Contact_Updates item, string jsonString)
        {
            var response = await api.PostAsync($@"https://api.getbase.com/v2/contacts", jsonString);
            if (response.IsSuccessStatusCode)
            {
                OnMessageRaised(this, $@"Created contact for {item.ContactEmail}");
                item.SentToZendeskIndicator = true;
                item.ZendeskResponseStatusCode = response.StatusCode.ToString();
            }
            else
                OnErrorMessageRaised(response);
        }
        private async Task UpdateContactAsync(Zendesk_Contact_Updates item, string jsonString, int contactId)
        {
            

            var response = await api.PutAsync($@"https://api.getbase.com/v2/contacts/{contactId}", jsonString);
            if (response.IsSuccessStatusCode)
            {
                OnMessageRaised(this, $@"Updated contact for {item.ContactEmail}");
                item.SentToZendeskIndicator = true;
                item.ZendeskResponseStatusCode = response.StatusCode.ToString();
            }
            else
                OnErrorMessageRaised(response);
        }

        private void OnErrorMessageRaised(HttpResponseMessage response)
        {
            OnMessageRaised(this, response.Content.ReadAsStringAsync().Result);
        }

        private async Task SendLeadUpdate(Zendesk_Contact_Updates item)
        {
            try
            {
                var jsonObject = new
                {
                    data = new
                    {
                        first_name = item.FirstName,
                        last_name = item.LastName == string.Empty ? "Unknown" : item.LastName,
                        email = item.ContactEmail,
                        phone = item.ContactNumber,
                        mobile = item.MobileNumber,
                        tags = new string[] { "Incomplete Signup" },
                        owner_id = leadRouterOwnerId != 0 ? leadRouterOwnerId:(int?)null,
                    },

                };
                string jsonString = JsonConvert.SerializeObject(jsonObject, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                //GET: Check if a lead already exists
                var response = await api.GetAsync($@"https://api.getbase.com/v2/leads?email={item.ContactEmail}");
                var data = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                if (data["items"].Count() == 0)
                {
                    //Create new lead
                    response = await api.PostAsync($@"https://api.getbase.com/v2/leads", jsonString);
                }
                else
                {
                    //Update existing lead
                    int leadId = (int)data["items"][0]["data"]["id"];
                    response = await api.PutAsync($@"https://api.getbase.com/v2/leads/{leadId}", jsonString);
                }
                if (response.IsSuccessStatusCode)
                {
                    OnMessageRaised(this, $@"Created lead for {item.ContactEmail}");
                    item.SentToZendeskIndicator = true;
                    item.ZendeskResponseStatusCode = response.StatusCode.ToString();
                }
                else
                {
                    item.SentToZendeskIndicator = false;
                    item.ZendeskResponseStatusCode = response.StatusCode.ToString();
                }

            }
            catch (Exception e)
            {
                OnMessageRaised(this, e.Message);
                throw e;
            }
        }




    }
}
