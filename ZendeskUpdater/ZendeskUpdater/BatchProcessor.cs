﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZendeskUpdater
{
    public class BatchProcessor<TDalItem>
    {
        private int skip = 0;
        private int batchsize = 0;
        public BatchProcessor(int batchsize)
        {
            this.batchsize = batchsize;
        }

        public void ProcessBatches(DbContext context, IQueryable<TDalItem> query, Func<TDalItem,Task> processTask)
        {
            var queryResults = query.ToList();
            skip = 0;


            var tasklist = new List<Task>();
            do
            {
                tasklist.Clear();
                foreach (var item in queryResults.Skip(skip).Take(batchsize))
                {
                    tasklist.Add((processTask(item)));
                }
                Task.WaitAll(tasklist.ToArray());

                skip += tasklist.Count();
                context.SaveChanges();
            }
            while (tasklist.Count == batchsize);
        }
    }
}
