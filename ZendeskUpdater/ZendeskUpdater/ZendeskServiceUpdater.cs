﻿using IntegrationDal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ZendeskUpdater
{
    public class ZendeskServiceUpdater:MessageObject
    {
        protected Entities Context;
        private int batchsize;
        private int leadRouterOwnerId;
        private int ContactRouterOwnerId;
        private ZendeskApi api;

        public ZendeskServiceUpdater(string userName, string zendeskToken, string connectionString, int batchsize)
        {
            this.batchsize = batchsize;
            
            DbConfiguration.SetConfiguration(new DalConfiguration());
            Context = Entities.CreateFromConnectionString(connectionString);

            api = new ZendeskApi(userName,zendeskToken);
            api.MessageRaised += Api_MessageRaised;

        }

        private void Api_MessageRaised(object sender, string e)
        {
            OnMessageRaised(sender, e);
        }

        

        private void OnErrorMessageRaised(HttpResponseMessage response)
        {
            OnMessageRaised(this, response.Content.ReadAsStringAsync().Result);
        }



        public void UpdateKycTickets()
        { 

            var query = from item in Context.Zendesk_Kyc_Updates
                        where item.SentToZendeskIndicator == false
                        orderby item.Id
                        select item;
            BatchProcessor<Zendesk_Kyc_Updates> processor = new BatchProcessor<Zendesk_Kyc_Updates>(batchsize);
            processor.ProcessBatches(Context, query, SendKycUpdate);
            
        }

        private async Task SendKycUpdate(Zendesk_Kyc_Updates item)
        {
            try
            {
                var response = await api.GetAsync($@"https://wealthpoint.zendesk.com/api/v2/search.json?query=requester:{item.Email} tags:kyc status<closed");
                var data = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                if (data["results"].Count() == 0)
                {
                    await CreateNewKycRequestTicket(api, item);
                }
                else
                {
                    await UpdateExistingKycRequestTicket(api, item, data);
                }

            }
            catch (Exception e)
            {
                OnMessageRaised(this, e.Message);
                throw e;
            }
        }

        private async Task UpdateExistingKycRequestTicket(ZendeskApi api, Zendesk_Kyc_Updates item, JObject data)
        {
            int ticketId = (int)data["results"][0]["id"];
            var jsonObject = new
            {
                ticket = new
                {
                    comment = new
                    {
                        body = $@"Entity for {item.Name} ({item.Email}) has been updated and marked as KYC Pending.",
                        @public = false,
                    },
                    subject = $@"Updated KYC Request for {item.Name} ({item.Email})",
                    
                },

            };
            string jsonString = JsonConvert.SerializeObject(jsonObject, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            var response = await api.PutAsync($@"https://wealthpoint.zendesk.com/api/v2/tickets/{ticketId}", jsonString);
            if (response.IsSuccessStatusCode)
            {
                OnMessageRaised(this, $@"Updated KYC Request Ticket: {item.Name} ({item.Email})");
                item.SentToZendeskIndicator = true;
                item.ZendeskResponseStatusCode = response.StatusCode.ToString();
            }
        }

        private async Task CreateNewKycRequestTicket(ZendeskApi api, Zendesk_Kyc_Updates item)
        {
            var jsonObject = new
            {
                ticket = new
                {
                    comment = new
                    {
                        body = $@"Entity for {item.Name} ({item.Email}) has been marked as KYC Pending.",
                        @public = false,
                    },
                    priority = "urgent",
                    subject = $@"KYC Request for {item.Name} ({item.Email})",
                    tags = new string[] { "kyc" },
                    type = "task",
                    requester = new
                    {
                        name = item.Name,
                        email = item.Email
                    }
                },

            };
            string jsonString = JsonConvert.SerializeObject(jsonObject, new JsonSerializerSettings
            { NullValueHandling = NullValueHandling.Ignore });

            var response = await api.PostAsync($@"https://wealthpoint.zendesk.com/api/v2/tickets.json", jsonString);
            if (response.IsSuccessStatusCode)
            {
                OnMessageRaised(this, $@"Created new KYC Request Ticket: {item.Name} ({item.Email})");
                item.SentToZendeskIndicator = true;
                item.ZendeskResponseStatusCode = response.StatusCode.ToString();
            }

        }


    }
}
