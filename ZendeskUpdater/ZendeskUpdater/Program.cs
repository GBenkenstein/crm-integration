﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZendeskUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            ZendeskSellUpdater sellUpdater = new ZendeskSellUpdater(ConfigurationManager.AppSettings["pwg_zendeskBearerToken"]
               , ConfigurationManager.ConnectionStrings["Entities"].ConnectionString
               , 10
               , ConfigurationManager.AppSettings["lead_router_name"]
               , ConfigurationManager.AppSettings["contact_router_name"]);

            sellUpdater.MessageRaised += Updater_MessageRaised;
            sellUpdater.ProcessUpdates();

            ZendeskServiceUpdater serviceUpdater = new ZendeskServiceUpdater(ConfigurationManager.AppSettings["wp_username"]
                , ConfigurationManager.AppSettings["wp_zendeskToken"]
                , ConfigurationManager.ConnectionStrings["Entities"].ConnectionString
                , 10);
            serviceUpdater.MessageRaised += Updater_MessageRaised;
            serviceUpdater.UpdateKycTickets();
        }

        private static void Updater_MessageRaised(object sender, string e)
        {
            Console.WriteLine(e);
        }
    }
}
