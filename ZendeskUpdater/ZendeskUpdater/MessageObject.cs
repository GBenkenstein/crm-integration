﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZendeskUpdater
{
    public abstract class MessageObject
    {
        public event EventHandler<string> MessageRaised;

        protected virtual void OnMessageRaised(object sender, string msg)
        {
            MessageRaised?.Invoke(sender, msg);
        }
    }
}
