TRUNCATE TABLE [WealthMigrateIntegration].Staging.EDWContact
TRUNCATE TABLE [WealthMigrateIntegration].[dbo].Ontraport_Contact_Updates

INSERT INTO [WealthMigrateIntegration].Staging.EDWContact
(
	  [Email]
	, [Last Name]
	, [First Name]
	, [Date Added]
	, [Contact Tags]
	, [ContactId]
	, [SubscribeToDealInfo]
	, [SubscribeToEvents]
	, [SubscribeToNewsletter]
	, [LegacyInvestorFlag]
	, [WealthUniversityFlag]
	, [PlatformMemberFlag]
	, [PlatformInvestorFlag]
	, [KycStatus]
)

SELECT [Email]
      ,[Last Name]
      ,[First Name]
      ,[Date Added]
      ,[Contact Tags]
      ,[ContactId]
	  , NULL
	  , NULL
	  , NULL
	  , 0
	  , 0
	  , 0
	  , 0
	  , NULL
  FROM [WealthMigrateEDW].[Staging].[Ontraport_Contacts]


DECLARE @RowsAdded INT;
EXEC [WealthMigrateIntegration].[dbo].[sp_PopulateOntraport_Contact_Updates]
@auditKey = -1
, @countRowsInserted = @RowsAdded OUTPUT

UPDATE [WealthMigrateIntegration].[dbo].Ontraport_Contact_Updates
SET SentToOntraportIndicator = 1