/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ontraport_Contact_Updates
	DROP CONSTRAINT DF__Ontraport__SentT__5CD6CB2B
GO
CREATE TABLE dbo.Tmp_Ontraport_Contact_Updates
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	Email nvarchar(500) NULL,
	[Last Name] nvarchar(500) NULL,
	[First Name] nvarchar(500) NULL,
	[Date Added] nvarchar(500) NULL,
	[Contact Tags] nvarchar(4000) NULL,
	ContactId nvarchar(500) NULL,
	SubscribeToDealInfo bit NULL,
	SubscribeToEvents bit NULL,
	SubscribeToNewsletter bit NULL,
	LegacyInvestorFlag bit NULL,
	WealthUniversityFlag bit NULL,
	PlatformMemberFlag bit NULL,
	PlatformInvestorFlag bit NULL,
	PlatformRepeatInvestorFlag bit NULL,
	KycStatus nvarchar(100) NULL,
	Country nvarchar(100) NULL,
	WalletBalance numeric(18, 2) NULL,
	AuditKey bigint NOT NULL,
	InsertDateTime datetime NOT NULL,
	CheckSum varbinary(20) NOT NULL,
	SentToOntraportIndicator bit NOT NULL,
	OntraportResponseStatusCode nvarchar(100) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Ontraport_Contact_Updates SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Ontraport_Contact_Updates ADD CONSTRAINT
	DF__Ontraport__SentT__5CD6CB2B DEFAULT ((0)) FOR SentToOntraportIndicator
GO
SET IDENTITY_INSERT dbo.Tmp_Ontraport_Contact_Updates ON
GO
IF EXISTS(SELECT * FROM dbo.Ontraport_Contact_Updates)
	 EXEC('INSERT INTO dbo.Tmp_Ontraport_Contact_Updates (Id, Email, [Last Name], [First Name], [Date Added], [Contact Tags], ContactId, SubscribeToDealInfo, SubscribeToEvents, SubscribeToNewsletter, LegacyInvestorFlag, WealthUniversityFlag, PlatformMemberFlag, PlatformInvestorFlag, PlatformRepeatInvestorFlag, KycStatus, Country, AuditKey, InsertDateTime, CheckSum, SentToOntraportIndicator, OntraportResponseStatusCode)
		SELECT Id, Email, [Last Name], [First Name], [Date Added], [Contact Tags], ContactId, SubscribeToDealInfo, SubscribeToEvents, SubscribeToNewsletter, LegacyInvestorFlag, WealthUniversityFlag, PlatformMemberFlag, PlatformInvestorFlag, PlatformRepeatInvestorFlag, KycStatus, Country, AuditKey, InsertDateTime, CheckSum, SentToOntraportIndicator, OntraportResponseStatusCode FROM dbo.Ontraport_Contact_Updates WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Ontraport_Contact_Updates OFF
GO
DROP TABLE dbo.Ontraport_Contact_Updates
GO
EXECUTE sp_rename N'dbo.Tmp_Ontraport_Contact_Updates', N'Ontraport_Contact_Updates', 'OBJECT' 
GO
ALTER TABLE dbo.Ontraport_Contact_Updates ADD CONSTRAINT
	PK_Ontraport_Contact_Updates PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE Staging.Tmp_EDWContact
	(
	Id bigint NOT NULL IDENTITY (1, 1),
	Email nvarchar(500) NULL,
	[Last Name] nvarchar(500) NULL,
	[First Name] nvarchar(500) NULL,
	[Date Added] nvarchar(500) NULL,
	[Contact Tags] nvarchar(4000) NULL,
	ContactId nvarchar(500) NULL,
	WalletBalance nvarchar(500) NULL,
	SubscribeToDealInfo bit NULL,
	SubscribeToEvents bit NULL,
	SubscribeToNewsletter bit NULL,
	LegacyInvestorFlag bit NULL,
	WealthUniversityFlag bit NULL,
	PlatformMemberFlag bit NULL,
	PlatformInvestorFlag bit NULL,
	PlatformRepeatInvestorFlag bit NULL,
	KycStatus nvarchar(100) NULL,
	Country nvarchar(100) NULL,
	CheckSum varbinary(20) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE Staging.Tmp_EDWContact SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT Staging.Tmp_EDWContact ON
GO
IF EXISTS(SELECT * FROM Staging.EDWContact)
	 EXEC('INSERT INTO Staging.Tmp_EDWContact (Id, Email, [Last Name], [First Name], [Date Added], [Contact Tags], ContactId, SubscribeToDealInfo, SubscribeToEvents, SubscribeToNewsletter, LegacyInvestorFlag, WealthUniversityFlag, PlatformMemberFlag, PlatformInvestorFlag, PlatformRepeatInvestorFlag, KycStatus, Country, CheckSum)
		SELECT Id, Email, [Last Name], [First Name], [Date Added], [Contact Tags], ContactId, SubscribeToDealInfo, SubscribeToEvents, SubscribeToNewsletter, LegacyInvestorFlag, WealthUniversityFlag, PlatformMemberFlag, PlatformInvestorFlag, PlatformRepeatInvestorFlag, KycStatus, Country, CheckSum FROM Staging.EDWContact WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT Staging.Tmp_EDWContact OFF
GO
DROP TABLE Staging.EDWContact
GO
EXECUTE sp_rename N'Staging.Tmp_EDWContact', N'EDWContact', 'OBJECT' 
GO
COMMIT
