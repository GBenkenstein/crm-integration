USE [WealthMigrateIntegration]
GO
/****** Object:  Schema [Staging]    Script Date: 2/12/2020 1:31:57 PM ******/
CREATE SCHEMA [Staging]
GO
/****** Object:  Table [dbo].[Audit]    Script Date: 2/12/2020 1:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Audit](
	[AuditKey] [bigint] IDENTITY(1,1) NOT NULL,
	[TargetTable] [nvarchar](500) NOT NULL,
	[PackageName] [nvarchar](500) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NULL,
	[StartRowCount] [int] NOT NULL,
	[EndRowCount] [int] NULL,
	[RowsInserted] [int] NULL,
	[RowsUpdated] [int] NULL,
	[RowsExpired] [int] NULL,
	[SuccessIndicator] [bit] NOT NULL,
 CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED 
(
	[AuditKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ontraport_Contact_Updates]    Script Date: 2/12/2020 1:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ontraport_Contact_Updates](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](500) NULL,
	[Last Name] [nvarchar](500) NULL,
	[First Name] [nvarchar](500) NULL,
	[Date Added] [nvarchar](500) NULL,
	[Contact Tags] [nvarchar](4000) NULL,
	[ContactId] [nvarchar](500) NULL,
	[SubscribeToDealInfo] [bit] NULL,
	[SubscribeToEvents] [bit] NULL,
	[SubscribeToNewsletter] [bit] NULL,
	[LegacyInvestorFlag] [bit] NULL,
	[WealthUniversityFlag] [bit] NULL,
	[PlatformMemberFlag] [bit] NULL,
	[PlatformInvestorFlag] [bit] NULL,
	[KycStatus] [nvarchar](100) NULL,
	[AuditKey] [bigint] NOT NULL,
	[InsertDateTime] [datetime] NOT NULL,
	[CheckSum] [varbinary](20) NOT NULL,
	[SentToOntraportIndicator] [bit] NOT NULL,
	[OntraportResponseStatusCode] [nvarchar](100) NULL,
 CONSTRAINT [PK_Ontraport_Contact_Updates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Staging].[EDWContact]    Script Date: 2/12/2020 1:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Staging].[EDWContact](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](500) NULL,
	[Last Name] [nvarchar](500) NULL,
	[First Name] [nvarchar](500) NULL,
	[Date Added] [nvarchar](500) NULL,
	[Contact Tags] [nvarchar](4000) NULL,
	[ContactId] [nvarchar](500) NULL,
	[SubscribeToDealInfo] [bit] NULL,
	[SubscribeToEvents] [bit] NULL,
	[SubscribeToNewsletter] [bit] NULL,
	[LegacyInvestorFlag] [bit] NULL,
	[WealthUniversityFlag] [bit] NULL,
	[PlatformMemberFlag] [bit] NULL,
	[PlatformInvestorFlag] [bit] NULL,
	[KycStatus] [nvarchar](100) NULL,
	[CheckSum] [varbinary](20) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Audit] ADD  CONSTRAINT [DF_Audit_RowsInserted]  DEFAULT ((0)) FOR [RowsInserted]
GO
ALTER TABLE [dbo].[Audit] ADD  CONSTRAINT [DF_Audit_RowsUpdated]  DEFAULT ((0)) FOR [RowsUpdated]
GO
ALTER TABLE [dbo].[Audit] ADD  CONSTRAINT [DF_Audit_RowsDeleted]  DEFAULT ((0)) FOR [RowsExpired]
GO
ALTER TABLE [dbo].[Audit] ADD  CONSTRAINT [DF_Audit_SuccessIndicator]  DEFAULT ((0)) FOR [SuccessIndicator]
GO
ALTER TABLE [dbo].[Ontraport_Contact_Updates] ADD  CONSTRAINT [DF__Ontraport__SentT__2B3F6F97]  DEFAULT ((0)) FOR [SentToOntraportIndicator]
GO
/****** Object:  StoredProcedure [dbo].[sp_PopulateOntraport_Contact_Updates]    Script Date: 2/12/2020 1:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_PopulateOntraport_Contact_Updates] 
	  @auditKey INT
	, @countRowsInserted INT OUTPUT
AS
BEGIN

	--Calculate Checksums
	UPDATE Staging.EDWContact
	SET CheckSum = HASHBYTES('SHA1',
				ISNULL(Email, '')+
				ISNULL([Last Name], '')+
				ISNULL([First Name], '')+
				ISNULL([Date Added], '')+
				ISNULL([Contact Tags], '')+
				ISNULL(CONVERT(NVARCHAR, [SubscribeToDealInfo]), '')+
				ISNULL(CONVERT(NVARCHAR, [SubscribeToEvents]), '')+
				ISNULL(CONVERT(NVARCHAR, [SubscribeToNewsletter]), '')+
				ISNULL(CONVERT(NVARCHAR, [LegacyInvestorFlag]), '')+
				ISNULL(CONVERT(NVARCHAR, [WealthUniversityFlag]), '')+
				ISNULL(CONVERT(NVARCHAR, [PlatformMemberFlag]), '')+
				ISNULL(CONVERT(NVARCHAR, [PlatformInvestorFlag]), '')+
				ISNULL(KycStatus, ''));

	

	WITH LatestDataSentToOntraport AS
	(
		SELECT * 
		FROM
		(
			SELECT [Id]
			  ,[Email]
			  ,[Last Name]
			  ,[First Name]
			  ,[Date Added]
			  ,[Contact Tags]
			  ,[ContactId]
			  ,[SubscribeToDealInfo]
			  ,[SubscribeToEvents]
			  ,[SubscribeToNewsletter]
			  ,[LegacyInvestorFlag]
			  ,[WealthUniversityFlag]
			  ,[PlatformMemberFlag]
			  ,[PlatformInvestorFlag]
			  ,[KycStatus]
			  ,[AuditKey]
			  ,[InsertDateTime]
			  ,[CheckSum]
			  ,[SentToOntraportIndicator]
			  , ROW_NUMBER() OVER (PARTITION BY Email ORDER BY InsertDateTime DESC) AS RowNum
		  FROM [WealthMigrateIntegration].[dbo].[Ontraport_Contact_Updates]
	  ) x
	  WHERE RowNum = 1
  )

	INSERT INTO [dbo].[Ontraport_Contact_Updates]
	(
		  [Email]
		, [Last Name]
		, [First Name]
		, [Date Added]
		, [Contact Tags]
		, [ContactId]
		, [SubscribeToDealInfo]
		, [SubscribeToEvents]
		, [SubscribeToNewsletter]
		, [LegacyInvestorFlag]
		, [WealthUniversityFlag]
		, [PlatformMemberFlag]
		, [PlatformInvestorFlag]
		, [KycStatus]
		, [AuditKey]
		, [InsertDateTime]
		, [CheckSum]
		, [SentToOntraportIndicator]
	)
	SELECT a.Email
		, a.[Last Name]
		, a.[First Name]
		, a.[Date Added]
		, a.[Contact Tags]
		, a.ContactId
		, a.SubscribeToDealInfo
		, a.SubscribeToEvents
		, a.SubscribeToNewsletter
		, a.LegacyInvestorFlag
		, a.WealthUniversityFlag
		, a.PlatformMemberFlag
		, a.PlatformInvestorFlag
		, a.KycStatus
		, @auditKey
		, GETDATE()
		, a.[Checksum]
		, 0
	FROM Staging.EDWContact a
	LEFT OUTER JOIN LatestDataSentToOntraport b ON b.CheckSum = a.CheckSum 
	WHERE b.CheckSum IS NULL

	SELECT @countRowsInserted = @@ROWCOUNT;
END
GO
