﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntraportUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            OntraportUpdater api = new OntraportUpdater(
                ConfigurationManager.ConnectionStrings["Entities"].ConnectionString,
                ConfigurationManager.AppSettings["AppId"],
                ConfigurationManager.AppSettings["ApiKey"]);

            api.MessageRaised += Api_MessageRaised;
            api.MergeContacts();

        }

        private static void Api_MessageRaised(object sender, string e)
        {
            Console.WriteLine(e);
        }
    }
}
