﻿using Newtonsoft.Json;
using IntegrationDal;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace OntraportUpdater
{
    public class OntraportUpdater :MessageObject
    {
        protected static OntraportApi Api; 
        protected Entities Context;

        public OntraportUpdater(string connectionString, string appId, string apiKey)
        {
            ServicePointManager.DefaultConnectionLimit = 500;
            DbConfiguration.SetConfiguration(new DalConfiguration());
            Context = Entities.CreateFromConnectionString(connectionString);
            Api = new OntraportApi(appId, apiKey);
            Api.MessageRaised += Api_MessageRaised;
        }

        private void Api_MessageRaised(object sender, string e)
        {
            OnMessageRaised(sender, e);
        }

        public void MergeContacts()
        {
            var query = from item in Context.Ontraport_Contact_Updates
                        where item.SentToOntraportIndicator == false 
                        orderby item.Id
                        select item;

            int batchsize = 10;
            int skip = 0;
            var queryResults = query.ToList();



            var tasklist = new List<Task<HttpResponseMessage>>();
            do
            {
                tasklist.Clear();
                foreach (var item in queryResults.Skip(skip).Take(batchsize))
                {
                    tasklist.Add((SendUpdate(item)));
                }
                Task.WaitAll(tasklist.ToArray());
                
                skip += tasklist.Count();
                Context.SaveChanges();
            }
            while (tasklist.Count == batchsize);
        }

        private async Task<HttpResponseMessage> SendUpdate(Ontraport_Contact_Updates item)
        {
            try
            {
                OnMessageRaised(this, string.Format("Sending contact data for {0}", item.Email));
                item.KycStatus = item.KycStatus ?? "Unverified";
                var jsonObject = new
                {
                    email = item.Email,
                    firstname = item.FirstName != null && item.FirstName.Trim().Length > 0 ? item.FirstName.Trim() : null,
                    lastname = item.LastName != null && item.LastName.Trim().Length > 0 ? item.LastName.Trim() : null,
                    country = item.Country,
                    cell_phone = item.MobileNumber,
                    home_phone = item.ContactNumber,
                    f1728 = item.KycStatus.Equals("KYC Approved") ? "151" : item.KycStatus.Equals("Pending Verification") ? "152" : "153",
                    f1611 = item.LegacyInvestorFlag.HasValue && item.LegacyInvestorFlag.Value ? 1 : 0,
                    f1604 = item.SubscribeToNewsletter.HasValue && item.SubscribeToNewsletter.Value ? 1 : 0,
                    f1605 = item.SubscribeToEvents.HasValue && item.SubscribeToEvents.Value ? 1 : 0,
                    f1606 = item.SubscribeToDealInfo.HasValue && item.SubscribeToDealInfo.Value ? 1 : 0,
                    f1614 = item.PlatformInvestorFlag.HasValue && item.PlatformInvestorFlag.Value ? 1 : 0,
                    f1613 = item.PlatformMemberFlag.HasValue && item.PlatformMemberFlag.Value ? 1 : 0,
                    f1749 = item.PlatformRepeatInvestorFlag.HasValue && item.PlatformRepeatInvestorFlag.Value ? 1 : 0,
                    f2048 = item.WalletBalance.HasValue ? item.WalletBalance.Value : 0,
                    f2050 = item.PlatformEntityId,
                };
                string jsonString = JsonConvert.SerializeObject(jsonObject, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                var response = await Api.PostAsync("https://api.ontraport.com/1/Contacts/saveorupdate", jsonString);
                if (response.IsSuccessStatusCode)
                {
                    item.Comment = string.Empty;
                    var data = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                    if (
                        !item.SubscribeToDealInfo.Value &&
                        !item.SubscribeToEvents.Value &&
                        !item.SubscribeToNewsletter.Value &&
                        data.Descendants().Where(x=> x.Type == JTokenType.Property && ((JProperty)x).Name == "attrs").FirstOrDefault() == null)// If a new contact is created this node is not present
                    {
                        var transactionalOnlyJsonObject = new
                        {
                            email = item.Email,
                            bulk_mail = "0"
                        };
                        jsonString = JsonConvert.SerializeObject(transactionalOnlyJsonObject, new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        });
                        var transactionalOnlyUpdateResponse = await Api.PostAsync("https://api.ontraport.com/1/Contacts/saveorupdate", jsonString);
                        if (transactionalOnlyUpdateResponse.IsSuccessStatusCode)
                        {
                            OnMessageRaised(this, string.Format("TransactionalOnly set for {0}", item.Email));
                            item.Comment = string.Format("TransactionalOnly set for {0}", item.Email);
                        }
                        else
                        {
                            item.Comment = await response.Content.ReadAsStringAsync();
                        }
                    }
                          
                    OnMessageRaised(this, string.Format("Data successfully sent for {0}", item.Email));
                    item.SentToOntraportIndicator = true;
                    
                }
                else
                {
                    item.Comment = await response.Content.ReadAsStringAsync();
                }
                item.OntraportResponseStatusCode = response.StatusCode.ToString();
                
                return response;
                
                
                
            }
            catch (Exception e)
            {
                OnMessageRaised(this, e.Message);
                throw e;
            }
        }
        
    }
}
