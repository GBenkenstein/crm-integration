﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OntraportUpdater
{
    public class OntraportApi : MessageObject
    {
        protected static HttpClient client = new HttpClient();


        public OntraportApi(string appId, string apiKey)
        {
            ServicePointManager.DefaultConnectionLimit = 500;

            client.DefaultRequestHeaders.Add("Api-Appid", appId);
            client.DefaultRequestHeaders.Add("Api-Key", apiKey);
        }

        public async Task<HttpResponseMessage> GetAsync(string requestUri)
        {
            var response = await client.GetAsync(requestUri);
            if (WaitIfNecessary(response))
                return await GetAsync(requestUri);
            
            return response;
        }

        public async Task<HttpResponseMessage> PostAsync(string requestUri, string content)
        {
            var response = await client.PostAsync(requestUri, new StringContent(content));
            if (WaitIfNecessary(response))
                return await PostAsync(requestUri, content);
            
            return response;
        }

        private bool WaitIfNecessary(HttpResponseMessage response)
        {
            int limitRemaining = int.Parse(response.Headers.GetValues("X-Rate-Limit-Remaining").First());
            int limitReset = int.Parse(response.Headers.GetValues("X-Rate-Limit-Reset").First());
            OnMessageRaised(this, $@"LimitRemaining: {limitRemaining} LimitReset: {limitReset}");
            if (limitRemaining <= 0)
            {
                OnMessageRaised(this, string.Format("Ontraport Rate Limit reached. Sleeping for {0} seconds", limitReset));
                Thread.Sleep(limitReset * 1000);
                return true;
            }
            return false;
        }

        public async Task<HttpResponseMessage> PutAsync(string requestUri, string content)
        {
            var response = await client.PutAsync(requestUri, new StringContent(content));
            if (WaitIfNecessary(response))
                return await PostAsync(requestUri, content);
            
            return response;
        }
    }
}
